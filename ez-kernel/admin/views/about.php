<?php

echo Bootstrap::pageTitle(array('title'=>$L->g('About'), 'icon'=>'info-circle'));

echo '
<table class="table table-striped mt-3">
	<tbody>
';
// do we _really_ want a so-called "pro" version?
echo '<tr>';
echo '<td>EzCMS Edition</td>';
if (defined('EZCMS_PRO')) {
	echo '<td>PRO - '.$L->g('Thanks for supporting EzCMS').' <span class="fa fa-heart" style="color: #ffc107"></span></td>';
} else {
	echo '<td>Standard - <a target="_blank" href="https://ezcms.net">'.$L->g('Upgrade to EzCMS PRO').'</a></td>';
}
echo '</tr>';

echo '<tr>';
echo '<td>EzCMS Version</td>';
echo '<td>'.EZCMS_VERSION.'</td>';
echo '</tr>';

echo '<tr>';
echo '<td>EzCMS Codename</td>';
echo '<td>'.EZCMS_CODENAME.'</td>';
echo '</tr>';

echo '<tr>';
echo '<td>EzCMS Build Number</td>';
echo '<td>'.EZCMS_BUILD.'</td>';
echo '</tr>';

echo '<tr>';
echo '<td>Disk usage</td>';
echo '<td>'.Filesystem::bytesToHumanFileSize(Filesystem::getSize(PATH_ROOT)).'</td>';
echo '</tr>';

echo '<tr>';
echo '<td><a href="'.HTML_PATH_ADMIN_ROOT.'developers'.'">EzCMS Developers</a></td>';
echo '<td></td>';
echo '</tr>';

echo '
	</tbody>
</table>
';
