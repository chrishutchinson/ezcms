<?php

echo Bootstrap::pageTitle(array('title'=>$L->g('Developers'), 'icon'=>'gears'));
echo '<h2 class="mb-4 mt-4">PHP version: <b>'.phpversion().'</b></h2>';
// PHP ini
$uploadOptions = array(
	'upload_max_filesize'=>ini_get('upload_max_filesize'),
	'post_max_size'=>ini_get('post_max_size'),
	'upload_tmp_dir'=>ini_get('upload_tmp_dir')
);
printTable('File Uploads', $uploadOptions);
// loaded extensions
printTable('Server information:', $_SERVER);
// PHP ini
printTable('PHP Configuration options:', ini_get_all());
// loaded extensions
printTable('Loaded extensions',get_loaded_extensions());
// locales installed
exec('locale -a', $locales);
printTable('Locales installed', $locales);
echo '<hr>';
echo '<h2>EZCMS</h2>';
echo '<hr>';
// constants defined by EzCMS
$constants = get_defined_constants(true);
printTable('EzCMS Constants', $constants['user']);
// site object
printTable('$site object database',$site->db);

