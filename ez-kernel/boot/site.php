<?php defined('EZCMS') or die('EzCMS CMS.');

// load plugins rules
include(PATH_RULES.'60.plugins.php');
// plugins before all
Theme::plugins('beforeAll');
// load rules
include(PATH_RULES.'60.router.php');
include(PATH_RULES.'69.pages.php');
include(PATH_RULES.'99.header.php');
include(PATH_RULES.'99.paginator.php');
include(PATH_RULES.'99.themes.php');
// plugins before site loaded
Theme::plugins('beforeSiteLoad');
// theme init.php
if (Sanitize::pathFile(PATH_THEMES, $site->theme().DS.'init.php')) {
	include(PATH_THEMES.$site->theme().DS.'init.php');
}
// theme HTML
if (Sanitize::pathFile(PATH_THEMES, $site->theme().DS.'index.php')) {
	include(PATH_THEMES.$site->theme().DS.'index.php');
} else {
	$L->p('Please check your theme configuration in the admin panel. Check for the active theme.');
}
// plugins after site loaded
Theme::plugins('afterSiteLoad');
// plugins after all
Theme::plugins('afterAll');
