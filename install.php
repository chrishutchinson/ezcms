<?php

/*
 * EzCMS https://ezcms.net
 * Author: Chris Hutchinson
 * License: MIT/BSD license
 */

// determine PHP ver
if (version_compare(phpversion(), '5.6', '<')) {
	$errorText = 'Current PHP version '.phpversion().', you need > 5.6.';
	error_log('[ERROR] '.$errorText, 0);
	exit($errorText);
}
// determine PHP modules
$modulesRequired = array('mbstring', 'json', 'gd', 'dom');
$modulesRequiredExit = false;
$modulesRequiredMissing = '';
foreach ($modulesRequired as $module) {
	if (!extension_loaded($module)) {
		$errorText = 'PHP module <b>'.$module.'</b> is not installed.';
		error_log('[ERROR] '.$errorText, 0);

		$modulesRequiredExit = true;
		$modulesRequiredMissing .= $errorText.PHP_EOL;
	}
}
if ($modulesRequiredExit) {
	echo 'PHP modules missing:';
	echo $modulesRequiredMissing;
	echo '';
	echo '<a href="https://ezcms.net/docs/en/getting-started/requirements">Please read EzCMS requirements</a>.';
	exit(0);
}
// security related constant
define('EZCMS', true);
// directory separator
define('DS', DIRECTORY_SEPARATOR);
// PHP paths
define('PATH_ROOT',		__DIR__.DS);
define('PATH_CONTENT',		PATH_ROOT.'ez-content'.DS);
define('PATH_KERNEL',		PATH_ROOT.'ez-kernel'.DS);
define('PATH_LANGUAGES',	PATH_ROOT.'ez-languages'.DS);
define('PATH_UPLOADS',		PATH_CONTENT.'uploads'.DS);
define('PATH_TMP',		PATH_CONTENT.'tmp'.DS);
define('PATH_PAGES',		PATH_CONTENT.'pages'.DS);
define('PATH_WORKSPACES',	PATH_CONTENT.'workspaces'.DS);
define('PATH_DATABASES',	PATH_CONTENT.'databases'.DS);
define('PATH_PLUGINS_DATABASES',PATH_CONTENT.'databases'.DS.'plugins'.DS);
define('PATH_UPLOADS_PROFILES',	PATH_UPLOADS.'profiles'.DS);
define('PATH_UPLOADS_THUMBNAILS',PATH_UPLOADS.'thumbnails'.DS);
define('PATH_UPLOADS_PAGES',	PATH_UPLOADS.'pages'.DS);
define('PATH_HELPERS',		PATH_KERNEL.'helpers'.DS);
define('PATH_ABSTRACT',		PATH_KERNEL.'abstract'.DS);
// protect against symlink attacks
define('CHECK_SYMBOLIC_LINKS', TRUE);
// filename for pages
define('FILENAME', 'index.txt');
// domain and protocol
define('DOMAIN', $_SERVER['HTTP_HOST']);

if (!empty($_SERVER['HTTPS'])) {
	define('PROTOCOL', 'https://');
} else {
	define('PROTOCOL', 'http://');
}
// base URL
// change the base URL or alolow EzCMS to try to do it
$base = '';

if (!empty($_SERVER['DOCUMENT_ROOT']) && !empty($_SERVER['SCRIPT_NAME']) && empty($base)) {
	$base = str_replace($_SERVER['DOCUMENT_ROOT'], '', $_SERVER['SCRIPT_NAME']);
	$base = dirname($base);
} elseif (empty($base)) {
	$base = empty( $_SERVER['SCRIPT_NAME'] ) ? $_SERVER['PHP_SELF'] : $_SERVER['SCRIPT_NAME'];
	$base = dirname($base);
}

if (strpos($_SERVER['REQUEST_URI'], $base)!==0) {
	$base = '/';
} elseif ($base!=DS) {
	$base = trim($base, '/');
	$base = '/'.$base.'/';
} else {
	// workaround for Windows web servers
	$base = '/';
}

define('HTML_PATH_ROOT', $base);
// log separator
define('LOG_SEP', ' | ');
// JSON
if (!defined('JSON_PRETTY_PRINT')) {
	define('JSON_PRETTY_PRINT', 128);
}
// database format date
define('DB_DATE_FORMAT', 'Y-m-d H:i:s');
// charset, default utf-8
define('CHARSET', 'utf-8');
// default language file
define('DEFAULT_LANGUAGE_FILE', 'en.json');
// set internal character encoding
mb_internal_encoding(CHARSET);
// set HTTP output character encoding
mb_http_output(CHARSET);
// directory perms
define('DIR_PERMISSIONS', 0755);
// PHP classes
include(PATH_ABSTRACT.'dbjson.class.php');
include(PATH_HELPERS.'sanitize.class.php');
include(PATH_HELPERS.'valid.class.php');
include(PATH_HELPERS.'text.class.php');
include(PATH_HELPERS.'log.class.php');
include(PATH_HELPERS.'date.class.php');
include(PATH_KERNEL.'language.class.php');
// LANGUAGE and LOCALE

// attempt detection of language from browser || headers
$languageFromHTTP = 'en';
$localeFromHTTP = 'en_US';

if (isset($_GET['language'])) {
	$languageFromHTTP = Sanitize::html($_GET['language']);
} else {
// attempt to detect the language from the browser
	$languageFromHTTP = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
// attempt to detect the locale
	if (function_exists('locale_accept_from_http')) {
		$localeFromHTTP = locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']);
	}
}

$finalLanguage = 'en';
$languageFiles = getLanguageList();
foreach ($languageFiles as $fname=>$native) {
	if ( ($languageFromHTTP==$fname) || ($localeFromHTTP==$fname) ) {
		$finalLanguage = $fname;
	}
}

$L = $language = new Language($finalLanguage);
// set locale
setlocale(LC_ALL, $localeFromHTTP);
// TIMEZONE

// determine whether the timezone is defined in php.ini
$iniDate = ini_get('date.timezone');
if (empty($iniDate)) {
	// timezone was not defined in php.ini, then set UTC as default.
	date_default_timezone_set('UTC');
}
// ============================================================================
// FUNCTIONS
// ============================================================================

// returns an array with all languages
function getLanguageList() {
	$files = glob(PATH_LANGUAGES.'*.json');
	$tmp = array();
	foreach ($files as $file) {
		$t = new dbJSON($file, false);
		$native = $t->db['language-data']['native'];
		$locale = basename($file, '.json');
		$tmp[$locale] = $native;
	}

	return $tmp;
}
// check whether EzCMS is already installed
function alreadyInstalled() {
	return file_exists(PATH_DATABASES.'site.php');
}
// determine write permissions and .htaccess file
function checkSystem()
{
	$output = array();
// attempt to create .htaccess
	$htaccessContent = 'AddDefaultCharset utf-8

<IfModule mod_rewrite.c>

# Enable rewrite rules
RewriteEngine on

# Base directory
RewriteBase '.HTML_PATH_ROOT.'

# Deny direct access to the next directories
RewriteRule ^ez-content/(databases|workspaces|pages|tmp)/.*$ - [R=404,L]

# All URL process by index.php
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*) index.php [PT,L]

</IfModule>
Options -Indexes
DirectoryIndex index.php index.xhtml index.html index.htm';

	if (!file_put_contents(PATH_ROOT.'.htaccess', $htaccessContent)) {
		if (!empty($_SERVER['SERVER_SOFTWARE'])) {
			$webserver = Text::lowercase($_SERVER['SERVER_SOFTWARE']);
			if (Text::stringContains($webserver, 'apache') || Text::stringContains($webserver, 'litespeed')) {
				$errorText = 'Missing file, upload the file .htaccess';
				error_log('[ERROR] '.$errorText, 0);
				array_push($output, $errorText);
			}
		}
	}
// determine whether mod_rewrite is available
	if (function_exists('apache_get_modules') ) {
		if (!in_array('mod_rewrite', apache_get_modules())) {
			$errorText = 'Module mod_rewrite is not installed or loaded.';
			error_log('[ERROR] '.$errorText, 0);
			array_push($output, $errorText);
		}
	}
// attempt to create the content directory
	@mkdir(PATH_CONTENT, DIR_PERMISSIONS, true);
// determine whether the content directory is writeable
	if (!is_writable(PATH_CONTENT)) {
		$errorText = 'Writing test failure, check directory "ez-content" permissions.';
		error_log('[ERROR] '.$errorText, 0);
		array_push($output, $errorText);
	}

	return $output;
}
// install EzCMS
function install($adminPassword, $timezone)
{
	global $L;

	if (!date_default_timezone_set($timezone)) {
		date_default_timezone_set('UTC');
	}

	$currentDate = Date::current(DB_DATE_FORMAT);
// ============================================================================
// Create directories
// ============================================================================

// directories for the initial pages
	$pagesToInstall = array('example-page-1-slug', 'example-page-2-slug', 'example-page-3-slug', 'example-page-4-slug');
	foreach ($pagesToInstall as $page) {
		if (!mkdir(PATH_PAGES.$L->get($page), DIR_PERMISSIONS, true)) {
			$errorText = 'ERROR while attempting to create the directory=>'.PATH_PAGES.$L->get($page);
			error_log('[ERROR] '.$errorText, 0);
		}
	}
// directories for the initial plugins
	$pluginsToInstall = array('tinymce', 'about', 'simple-stats', 'robots', 'canonical');
	foreach ($pluginsToInstall as $plugin) {
		if (!mkdir(PATH_PLUGINS_DATABASES.$plugin, DIR_PERMISSIONS, true)) {
			$errorText = 'An ERROR occurred while attempting to create the directory=>'.PATH_PLUGINS_DATABASES.$plugin;
			error_log('[ERROR] '.$errorText, 0);
		}
	}
// directories for the upload files
	if (!mkdir(PATH_UPLOADS_PROFILES, DIR_PERMISSIONS, true)) {
		$errorText = 'An ERROR occurred while attempting to create the directory=>'.PATH_UPLOADS_PROFILES;
		error_log('[ERROR] '.$errorText, 0);
	}

	if (!mkdir(PATH_UPLOADS_THUMBNAILS, DIR_PERMISSIONS, true)) {
		$errorText = 'An ERROR occurred while attempting to create the directory=>'.PATH_UPLOADS_THUMBNAILS;
		error_log('[ERROR] '.$errorText, 0);
	}

	if (!mkdir(PATH_TMP, DIR_PERMISSIONS, true)) {
		$errorText = 'An ERROR occurred while attempting to create the directory=>'.PATH_TMP;
		error_log('[ERROR] '.$errorText, 0);
	}

	if (!mkdir(PATH_WORKSPACES, DIR_PERMISSIONS, true)) {
		$errorText = 'An ERROR occurred while attempting to create the directory=>'.PATH_WORKSPACES;
		error_log('[ERROR] '.$errorText, 0);
	}

	if (!mkdir(PATH_UPLOADS_PAGES, DIR_PERMISSIONS, true)) {
		$errorText = 'An ERROR occurred while attempting to create the directory=>'.PATH_UPLOADS_PAGES;
		error_log('[ERROR] '.$errorText, 0);
	}
// ============================================================================
// create the files
// ============================================================================
	$dataHead = "<?php defined('EZCMS') or die('EzCMS CMS.'); ?>".PHP_EOL;

	$data = array();
	$slugs = array();
	$nextDate = $currentDate;
	foreach ($pagesToInstall as $page) {

		$slug = $page;
		$title = Text::replace('slug','title', $slug);
		$content = Text::replace('slug','content', $slug);
		$nextDate = Date::offset($nextDate, DB_DATE_FORMAT, '-1 minute');

		$data[$L->get($slug)]= array(
			'title'=>$L->get($title),
			'description'=>'',
			'username'=>'admin',
			'tags'=>array(),
			'type'=>(($slug=='example-page-4-slug')?'static':'published'),
			'date'=>$nextDate,
			'dateModified'=>'',
			'allowComments'=>true,
			'position'=>1,
			'coverImage'=>'',
			'md5file'=>'',
			'category'=>'general',
			'uuid'=>md5(uniqid()),
			'parent'=>'',
			'template'=>'',
			'noindex'=>false,
			'nofollow'=>false,
			'noarchive'=>false
		);

		array_push($slugs, $slug);

		file_put_contents(PATH_PAGES.$L->get($slug).DS.FILENAME, $L->get($content), LOCK_EX);
	}
	file_put_contents(PATH_DATABASES.'pages.php', $dataHead.json_encode($data, JSON_PRETTY_PRINT), LOCK_EX);

// site.php

	// if EzCMS is not already installed inside a folder, the URL doesn't need end with a /
	// example (root): https://domain.com
	// example (inside a folder): https://domain.com/folder/
	if (HTML_PATH_ROOT=='/') {
		$siteUrl = PROTOCOL.DOMAIN;
	} else {
		$siteUrl = PROTOCOL.DOMAIN.HTML_PATH_ROOT;
	}
	$data = array(
		'title'=>'EZCMS',
		'slogan'=>$L->get('welcome-to-ezcms'),
		'description'=>$L->get('congratulations-you-have-successfully-installed-your-own-copy-of-ezcms'),
		'footer'=>'© Copyright '.Date::current('Y'),
		'itemsPerPage'=>6,
		'language'=>$L->currentLanguage(),
		'locale'=>$L->locale(),
		'timezone'=>$timezone,
		'theme'=>'derivative',
		'adminTheme'=>'booty',
		'homepage'=>'',
		'pageNotFound'=>'',
		'uriPage'=>'/',
		'uriTag'=>'/tag/',
		'uriCategory'=>'/category/',
		'uriBlog'=>'',
		'url'=>$siteUrl,
		'emailFrom'=>'no-reply@'.DOMAIN,
		'orderBy'=>'date',
		'currentBuild'=>'0',
		'twitter'=>'',
		'facebook'=>'',
		'codepen'=>'',
		'gitlab'=> 'https://gitlab.com/chrishutchinson/ezcms',
		'instagram'=>'',
		'gitlab'=>'',
		'linkedin'=>'',
		'dateFormat'=>'F j, Y',
		'extremeFriendly'=>true,
		'autosaveInterval'=>2,
		'titleFormatHomepage'=>'{{site-slogan}} | {{site-title}}',
		'titleFormatPages'=>'{{page-title}} | {{site-title}}',
		'titleFormatCategory'=>'{{category-name}} | {{site-title}}',
		'titleFormatTag'=>'{{tag-name}} | {{site-title}}',
		'imageRestrict'=>true,
		'imageRelativeToAbsolute'=>false
	);
	file_put_contents(PATH_DATABASES.'site.php', $dataHead.json_encode($data, JSON_PRETTY_PRINT), LOCK_EX);
// users.php
	$salt = uniqid();
	$passwordHash = sha1($adminPassword.$salt);
	$tokenAuth = md5( uniqid().time().DOMAIN );

	$data = array(
		'admin'=>array(
			'nickname'=>'Admin',
			'firstName'=>$L->get('Administrator'),
			'lastName'=>'',
			'role'=>'admin',
			'password'=>$passwordHash,
			'salt'=>$salt,
			'email'=>'',
			'registered'=>$currentDate,
			'tokenRemember'=>'',
			'tokenAuth'=>$tokenAuth,
			'tokenAuthTTL'=>'2009-03-15 14:00',
			'twitter'=>'',
			'facebook'=>'',
			'instagram'=>'',
			'codepen'=>'',
			'linkedin'=>'',
			'github'=>'',
			'gitlab'=>''
		)
	);
	file_put_contents(PATH_DATABASES.'users.php', $dataHead.json_encode($data, JSON_PRETTY_PRINT), LOCK_EX);
// syslog.php
	$data = array(
		array(
			'date'=>$currentDate,
			'dictionaryKey'=>'welcome-to-ezcms',
			'notes'=>'',
			'idExecution'=>uniqid(),
			'method'=>'POST',
			'username'=>'admin'
	));
	file_put_contents(PATH_DATABASES.'syslog.php', $dataHead.json_encode($data, JSON_PRETTY_PRINT), LOCK_EX);
// security.php
	$data = array(
		'minutesBlocked'=>5,
		'numberFailuresAllowed'=>10,
		'blackList'=>array()
	);
	file_put_contents(PATH_DATABASES.'security.php', $dataHead.json_encode($data, JSON_PRETTY_PRINT), LOCK_EX);
// categories.php
	$data = array(
		'general'=>array('name'=>'General', 'description'=>'', 'template'=>'', 'list'=>$slugs),
		'music'=>array('name'=>'Music', 'description'=>'', 'template'=>'', 'list'=>array()),
		'videos'=>array('name'=>'Videos', 'description'=>'', 'template'=>'', 'list'=>array())
	);
	file_put_contents(PATH_DATABASES.'categories.php', $dataHead.json_encode($data, JSON_PRETTY_PRINT), LOCK_EX);
// tags.php
	$data = array(
		'ezcms'=>array('name'=>'EzCMS', 'description'=>'', 'template'=>'', 'list'=>array('follow-ezcms')),
		'cms'=>array('name'=>'CMS', 'description'=>'', 'template'=>'', 'list'=>array('follow-ezcms')),
		'flat-files'=>array('name'=>'Flat files', 'description'=>'', 'template'=>'', 'list'=>array('follow-ezcms'))
	);
	file_put_contents(PATH_DATABASES.'tags.php', $dataHead.json_encode($data, JSON_PRETTY_PRINT), LOCK_EX);

// plugins/about/db.php
	file_put_contents(
		PATH_PLUGINS_DATABASES.'about'.DS.'db.php',
		$dataHead.json_encode(
			array(
				'position'=>1,
				'label'=>$L->get('About'),
				'text'=>$L->get('this-is-a-brief-description-of-yourself-our-your-site')
			),
		JSON_PRETTY_PRINT),
		LOCK_EX
	);
// plugins/simple-stats/db.php
	file_put_contents(
		PATH_PLUGINS_DATABASES.'simple-stats'.DS.'db.php',
		$dataHead.json_encode(
			array(
				'numberOfDays'=>7,
				'label'=>$L->get('Visits'),
				'excludeAdmins'=>false,
				'position'=>1
			),
		JSON_PRETTY_PRINT),
		LOCK_EX
	);
	mkdir(PATH_WORKSPACES.'simple-stats', DIR_PERMISSIONS, true);
// plugins/tinymce/db.php
	file_put_contents(
		PATH_PLUGINS_DATABASES.'tinymce'.DS.'db.php',
		$dataHead.json_encode(
			array(
				'position'=>1,
				'toolbar1'=>'formatselect bold italic forecolor backcolor removeformat | bullist numlist table | blockquote alignleft aligncenter alignright | link unlink pagebreak image code',
				'toolbar2'=>'',
				'plugins'=>'code autolink image link pagebreak advlist lists textpattern table'
			),
		JSON_PRETTY_PRINT),
		LOCK_EX
	);
// plugins/canonical/db.php
	file_put_contents(
		PATH_PLUGINS_DATABASES.'canonical'.DS.'db.php',
		$dataHead.json_encode(
			array(
				'position'=>1
			),
		JSON_PRETTY_PRINT),
		LOCK_EX
	);
// plugins/robots/db.php
	file_put_contents(
		PATH_PLUGINS_DATABASES.'robots'.DS.'db.php',
		$dataHead.json_encode(
			array(
				'position'=>1,
				'robotstxt'=>'User-agent: *'.PHP_EOL.'Allow: /'
			),
		JSON_PRETTY_PRINT),
		LOCK_EX
	);

	return true;
}

function redirect($url) {
	if (!headers_sent()) {
		header("Location:".$url, TRUE, 302);
		exit;
	}

	exit('<meta http-equiv="refresh" content="0; url="'.$url.'">');
}

// ============================================================================
// MAIN
// ============================================================================

if (alreadyInstalled()) {
	$errorText = 'EzCMS is already installed ;)';
	error_log('[ERROR] '.$errorText, 0);
	exit($errorText);
}
// install a demo
// just call the install.php?demo=true
if (isset($_GET['demo'])) {
	install('demo123', 'UTC');
	redirect(HTML_PATH_ROOT);
}
// install by POST method
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if (Text::length($_POST['password'])<8) {
		$errorText = $L->g('password-must-be-at-least-8-characters-long');
		error_log('[ERROR] '.$errorText, 0);
	} else {
		install($_POST['password'], $_POST['timezone']);
		redirect(HTML_PATH_ROOT);
	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $L->get('EzCMS Installer') ?></title>
	<meta charset="<?php echo CHARSET ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="robots" content="noindex,nofollow">
	<!--favicon-->
	<link rel="icon" type="image/png" href="ez-kernel/img/favicon.png?version=<?php echo time() ?>">
	<!--css-->
	<link rel="stylesheet" type="text/css" href="ez-kernel/css/bootstrap.min.css?version=<?php echo time() ?>">
	<link rel="stylesheet" type="text/css" href="ez-kernel/admin/themes/booty/css/ezcms.css?version=<?php echo time() ?>">
	<!--javascript-->
	<script charset="utf-8" src="ez-kernel/js/jquery.min.js?version=<?php echo time() ?>"></script>
	<script charset="utf-8" src="ez-kernel/js/bootstrap.bundle.min.js?version=<?php echo time() ?>"></script>
	<script charset="utf-8" src="ez-kernel/js/jstz.min.js?version=<?php echo time() ?>"></script>
</head>
<body class="login">
<div class="container">
	<div class="row justify-content-md-center pt-5">
		<div class="col-md-4 pt-5">
			<h1 class="text-center mb-5 mt-5 font-weight-normal text-uppercase" style="color: #555;"><?php echo $L->get('EzCMS Installer') ?></h1>
			<?php
			$system = checkSystem();
			if (!empty($system)) {
				foreach ($system as $error) {
					echo '
					<table class="table">
						<tbody>
							<tr>
								<th>'.$error.'</th>
							</tr>
						</tbody>
					</table>
					';
				}
			}
			elseif (isset($_GET['language']))
			{
			?>
				<p><?php echo $L->get('choose-a-password-for-the-admin-user') ?></p>

				<?php if (!empty($errorText)): ?>
				<div class="alert alert-danger"><?php echo $errorText ?></div>
				<?php endif ?>
<!-- FIXME -->
				<form id="jsformInstaller" method="post" action="" autocomplete="off">
					<input type="hidden" name="timezone" id="jstimezone" value="UTC">

					<div class="form-group">
					<input type="text" value="admin" class="form-control form-control-lg" id="jsusername" name="username" placeholder="Username" disabled>
					</div>

					<div class="form-group mb-0">
					<input type="password" class="form-control form-control-lg" id="jspassword" name="password" placeholder="<?php $L->p('Password') ?>">
					</div>
					<div id="jsshowPassword" style="cursor: pointer;" class="text-center pt-0 text-muted"><?php $L->p('Show password') ?></div>

					<div class="form-group mt-4">
					<button type="submit" class="btn btn-primary btn-lg mr-2 w-100" name="install"><?php $L->p('Install') ?></button>
					</div>
				</form>
			<?php
			}
			else
			{
			?>
				<form id="jsformLanguage" method="get" action="" autocomplete="off">
					<label for="jslanguage"><?php echo $L->get('Choose your language') ?></label>
					<select id="jslanguage" name="language" class="form-control form-control-lg">
					<?php
						$htmlOptions = getLanguageList();
						foreach($htmlOptions as $fname=>$native) {
							echo '<option value="'.$fname.'"'.( ($finalLanguage===$fname)?' selected="selected"':'').'>'.$native.'</option>';
						}
					?>
					</select>

					<div class="form-group mt-4">
					<button type="submit" class="btn btn-primary btn-lg mr-2 w-100"><?php $L->p('Next') ?></button>
					</div>
				</form>
			<?php
			}
			?>
		</div>
	</div>
</div>

<script>
$(document).ready(function()
{
// timezone
	var timezone = jstz.determine();
	$("#jstimezone").val( timezone.name() );
// show password
	$("#jsshowPassword").on("click", function() {
		var input = document.getElementById("jspassword");

		if(input.getAttribute("type")=="text") {
			input.setAttribute("type", "password");
		}
		else {
			input.setAttribute("type", "text");
		}
	});

});
</script>

</body></html>
