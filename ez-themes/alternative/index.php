<!DOCTYPE html>
<html lang="<?php echo Theme::lang() ?>">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="generator" content="EzCMS | https://ezcms.net">
	<!--dynamic title-->
	<?php echo Theme::metaTagTitle(); ?>
	<!--dynamic description-->
	<?php echo Theme::metaTagDescription(); ?>
	<!--favicon-->
	<?php echo Theme::favicon('img/favicon.png'); ?>
	<!--bootstrap css from EzCMS core-->
	<?php echo Theme::cssBootstrap(); ?>
	<!--styles from this theme-->
	<?php echo Theme::css('css/style.css'); ?>
	<!--EzCMS Plugins: site head-->
	<?php Theme::plugins('siteHead'); ?>
</head>
<body>
	<!--EzCMS Plugins: site body begin-->
	<?php Theme::plugins('siteBodyBegin'); ?>
	<!--navbar-->
	<?php include(THEME_DIR_PHP.'navbar.php'); ?>
<!--content-->
	<?php
		// $WHERE_AM_I : detects where the user is browsing:
		// if user is watching a particular page; variable takes value "page"
		// if user is watching the frontpage; variable takes value "home"
		if ($WHERE_AM_I == 'page') {
			include(THEME_DIR_PHP.'page.php');
		} else {
			include(THEME_DIR_PHP.'home.php');
		}
	?>
	<!--footer-->
	<?php include(THEME_DIR_PHP.'footer.php'); ?>
	<!--jquery from EzCMS core-->
	<?php echo Theme::jquery(); ?>
	<!--bootstrap javascript from EzCMS Core-->
	<?php echo Theme::jsBootstrap(); ?>
	<!--EzCMS Plugins: site body end-->
	<?php Theme::plugins('siteBodyEnd'); ?>
</body></html>
