[EzCMS](https://EzCMS.net/)
====
**Simple**, **Fast** and **Flexible** CMS.

EzCMS is a web application to build your own **website** or **blog** in just seconds. It's completely **free** and **opensource**. [EzCMS](https://ezcms.net) uses files written in JSON format to store content. You won't need to install, or configure a database. You only need a web server that supports PHP.

EzCMS is a **Flat-File-Based** CMS.

[EzCMS](https://ezcms.net) supports [Markdown](https://daringfireball.net/projects/markdown/) or **HTML markup** for content.

- [Plugins](https://EzCMS.net/plugins)
- [Themes](https://EzCMS.net/themes)
- [Documentation](https://EzCMS.net/docs)
- Help and Support [Forum](https://forum.EzCMS.net)

Follow EzCMS
---------------

- [Blog](https://EzCMS.net/blogs)

Requirements
------------

You just need a web server with PHP support.

- PHP v5.6 or higher (7.x recommended).
- PHP [mbstring](http://php.net/manual/en/book.mbstring.php) module for full UTF-8 support.
- PHP [gd](http://php.net/manual/en/book.image.php) module for image processing.
- PHP [dom](http://php.net/manual/en/book.dom.php) module for DOM manipulation.
- PHP [json](http://php.net/manual/en/book.json.php) module for JSON manipulation.
- Supported web servers:
   * EzCMS supports nearly **any** web server
   * or PHP Built-in web server (`php -S localhost:8000`)

Installation Guide
------------------

1. Download the latest version from the official page: [EzCMS.net](https://EzCMS.net/download) or the
source from [GitLab](https://gitlab.com/chrishutchinson/ezcms)
2. Extract the archive into a directory such as `ezcms`.
3. Upload the directory `ezcms`, or it's contents into your web site or at your hosting service.
4. Visit your domain `https://example.com/ezcms/` , or `https://example.com/` as appropriate.
5. Follow the EzCMS Installer to configure the web site.

Support EzCMS!
-------
[EzCMS](https://ezcms.net) is free, and open soruce software. But we'll need your help to continue it's development. Please support us with a donation of your time, or money.

Thank you

License
-------
[EzCMS](https://ezcms.net) is open source software licensed under the [MIT/BSD license](https://opensource.org/licenses/BSDplusPatent).
