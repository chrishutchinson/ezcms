
function getLatestVersion() {

	console.log("[INFO] [PLUGIN VERSION] Getting list of versions of EzCMS.");

	$.ajax({
		url: "https://ezcms.net/version/",
		method: "GET",
		dataType: 'json',
		success: function(json) {
			// Constant EZCMS_BUILD is defined on variables.js
			if (json.stable.build > EZCMS_BUILD) {
				$("#current-version").hide();
				$("#new-version").show();
			}
		},
		error: function(json) {
			console.log("[WARN] [PLUGIN VERSION] There is some issue to get the version status.");
		}
	});
}

getLatestVersion();