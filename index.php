<?php

/*
 * EzCMS https://ezcms.net
 * author: Chris Hutchinson
 * license: MIT/BSD license
 */

// determine whether EzCMS is already installed
if (!file_exists('ez-content/databases/site.php')) {
	$base = dirname($_SERVER['SCRIPT_NAME']);
	$base = rtrim($base, '/');
	$base = rtrim($base, '\\'); // Workaround for Windows Servers
	header('Location:'.$base.'/install.php');
	exit('<a href="./install.php">Install EzCMS first.</a>');
}
// load time init
$loadTime = microtime(true);
// security related constant
define('EZCMS', true);
// directory separator
define('DS', DIRECTORY_SEPARATOR);
// PHP paths for init
define('PATH_ROOT', __DIR__.DS);
define('PATH_BOOT', PATH_ROOT.'ez-kernel'.DS.'boot'.DS);
// init
require(PATH_BOOT.'init.php');
// the admin area
if ($url->whereAmI()==='admin') {
	require(PATH_BOOT.'admin.php');
}
// the site
else {
	require(PATH_BOOT.'site.php');
}
